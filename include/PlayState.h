#ifndef PlayState_H
#define PlayState_H

#define NUM_VEGETABLES 2
#define MAX_FUERZA 40.0
#define MIN_FUERZA 10.0
#define MAX_LEVEL  3
#define INC_FUERZA 0.75

#include <Ogre.h>
#include <OIS/OIS.h>

#include <OgreBulletDynamicsRigidBody.h>

#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include <Shapes/OgreBulletCollisionsConvexHullShape.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>

#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h> 

#include <stdlib.h>     
#include <time.h>       

#include "AnimalsManager.h"
#include "GameState.h"
#include "TrackManager.h"
#include "SoundFXManager.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void finPartida();
  void updateOvPower();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  TrackManager* _pTrackManager;
  SoundFXManager* _pSoundFXManager;

  SoundFXPtr _disparo, _llorar;

  Ogre::OverlayManager* _overlayManager;
  Ogre::ParticleSystem* _smoke;

 private:
  bool _exitGame, _endGame, _rPress, _uPress, _dPress, _lPress, _sPress;

  float _fuerza;
  int _level, _numEntities, _score, _savedSheeps;
  Ogre::Real _tiempo;

  OgreBulletDynamics::DynamicsWorld * _world;
  OgreBulletCollisions::DebugDrawer * _debugDrawer;

  Ogre::Overlay *_ovScore, *_ovPower;
  Ogre::OverlayElement *_o_score, *_o_sheeps, **_o_power;

  Ogre::Vector3 _dir;

  std::deque <OgreBulletDynamics::RigidBody *>         _bodies;
  std::deque <OgreBulletCollisions::CollisionShape *>  _shapes;

  Ogre::Real _deltaT;
  
  Ogre::SceneNode *_nodCannion, *_nodBase, *_nodShoot;
  Ogre::AnimationState* _animCannion;

  void createScene();
  void createOverlay();
  void createInitialWorld();
  void DetectCollisionDrain();
  void deleteSlept();
  void shootVegetable();
  void updateText();

};

#endif
