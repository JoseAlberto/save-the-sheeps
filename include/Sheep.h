#ifndef SHEEP
#define SHEEP

#include <Ogre.h>

#include "Animal.h"

class Sheep: public Animal{
public:
	Sheep(Ogre::SceneNode *node, Ogre::AnimationState *walk, 
	Ogre::AnimationState *stop, Ogre::AnimationState *jump,
	Ogre::AnimationState *dead);

	void update(Ogre::Real deltaT);
	void setAnimation(int state);
	void restart();
	void finish();

	int getPoints();
	int getState();
	void incPoints();

private:
	int _points;
	
	Ogre::AnimationState *_animWalk;
	Ogre::AnimationState *_animStop;
	Ogre::AnimationState *_animJump;
	Ogre::AnimationState *_animDead;
};
#endif
