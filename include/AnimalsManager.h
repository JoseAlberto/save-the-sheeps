#ifndef ANIMALS_MANAGER
#define ANIMALS_MANAGER

#define NUM_ELEMENTS 3
#define GRASS_TIME   5.0

#include <Ogre.h>
#include <OgreSingleton.h>

#include <OgreBulletDynamicsRigidBody.h>

#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include <Shapes/OgreBulletCollisionsConvexHullShape.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>

#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include <stdlib.h>     
#include <time.h> 
#include <vector>

#include "Actions.h"
#include "Wolf.h"
#include "Sheep.h"

#include "TrackManager.h"
#include "SoundFXManager.h"

class AnimalsManager: public Ogre::Singleton<AnimalsManager>{
public:
	AnimalsManager();

	void init(Ogre::SceneManager* mgr, OgreBulletDynamics::DynamicsWorld* world);
	void createAnimals();

	void update(Ogre::Real deltaT);
	void updateBodies();
	void restartWolf(int wolf);
	void start(int level);
	void stop();
	void clear();

	int getSaveSheeps();
	void setSaveSheeps(int saved);
	int getDieSheeps();

	void loadConfig();

	static AnimalsManager& getSingleton ();
  	static AnimalsManager* getSingletonPtr ();

private:
	int _level, _sSheeps, _dSheeps;
	bool _run;

	Ogre::Vector3 _sheepEnd;
	
	Ogre::SceneManager* _sceneMgr;
	OgreBulletDynamics::DynamicsWorld* _world;

	std::deque <OgreBulletDynamics::RigidBody*>         _wBodies;
  	std::deque <OgreBulletCollisions::CollisionShape*>  _wShapes;

  	std::vector<Ogre::Real> _time;
  	std::vector<Ogre::Vector3> _grassPos;
  	std::vector<Ogre::Vector3> _wolvesPos;

	std::vector<Sheep*> _sheeps;
	std::vector<Wolf*> _wolves;
	std::vector<Ogre::SceneNode*> _grass;

	SoundFXManager* _pSoundFXManager;
	SoundFXPtr _grunir, _salvada, _balar;
};

#endif
