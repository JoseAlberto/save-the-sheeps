#ifndef PauseState_H
#define PauseState_H


#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#include "GameState.h"

class PauseState : public Ogre::Singleton<PauseState>, public GameState
{
 public:
  PauseState() {
	  _creado=false;
  }

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);
  void createGUI();

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

  bool backMenu(const CEGUI::EventArgs &e);
  bool continueGame(const CEGUI::EventArgs &e);
  void createOverlay ();
  void attachButtons();

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  static PauseState& getSingleton ();
  static PauseState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  CEGUI::Window* sheet;
  CEGUI::Window* _continueButton;
  CEGUI::Window* _backButton;

  Ogre::OverlayManager* _overlayManager;
  Ogre::Overlay *_ovPause;



  bool _exitGame, _creado;
};

#endif
