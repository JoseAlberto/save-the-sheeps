#ifndef ANIMAL
#define ANIMAL

#define VELOCIDAD 4.5

#include <Ogre.h>

#include "Actions.h"

class Animal{
public:
	Animal(Ogre::SceneNode *node);

	virtual void update(Ogre::Real deltaT) = 0;
	virtual void setAnimation(int state) = 0;
	void changeObjetive(Ogre::Vector3 pos);
	Ogre::Vector3 getObjetive();
	virtual void restart() = 0;
	virtual void finish() = 0;
	
	Ogre::SceneNode* getNode();
protected:
	int _state;

	Ogre::Vector3 _dir, _objetive;
	Ogre::SceneNode *_node;
};
#endif