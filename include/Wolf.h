#ifndef WOLF
#define WOLF

#include <Ogre.h>

#include <stdlib.h> 
#include <vector>

#include "Animal.h"
#include "Sheep.h"


class Wolf: public Animal{
public:
	Wolf(Ogre::SceneNode *node, /*Ogre::SceneNode *box,*/ Ogre::AnimationState *walk,
	Ogre::AnimationState *stop, Ogre::AnimationState *run,
	Ogre::AnimationState *attack, Ogre::AnimationState *dead);
	~Wolf();

	void update(Ogre::Real deltaT);
	void setAnimation(int state);
	void restart();
	void finish();
	Ogre::SceneNode * getBox();
	bool isHungry();

	void setRandomPosition();
	void addPositions(std::vector<Ogre::Vector3> v);
	int getState();
	void setSheep(Sheep *mySheep);

private:
	bool _hungry;

	std::vector<Ogre::Vector3> _ioPos;

	Ogre::AnimationState *_animWalk;
	Ogre::AnimationState *_animStop;
	Ogre::AnimationState *_animRun;
	Ogre::AnimationState *_animAttack;
	Ogre::AnimationState *_animDead;
	Ogre::SceneNode *_boxCollision;

	Sheep *_sheep;

};

#endif
