#ifndef IntroState_H
#define IntroState_H

#include <Ogre.h>
#include <vector>
#include <OIS/OIS.h>

#include "GameState.h"

class IntroState : public Ogre::Singleton<IntroState>, public GameState
{
 public:
  IntroState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  static IntroState& getSingleton ();
  static IntroState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::SceneNode *_lobo;
  Ogre::AnimationState* _animLobo;
  std::vector<Ogre::SceneNode*> _ovejas;
  std::vector<Ogre::AnimationState*> _animaciones;

  Ogre::OverlayManager* _overlayManager;

  TrackManager* _pTrackManager;
  TrackPtr _mainTrack;

 private:
  bool _exitGame;

  Ogre::Real _time;
  Ogre::Overlay *_ovIntro;

  void createScene();
  void createOverlay();
};

#endif
