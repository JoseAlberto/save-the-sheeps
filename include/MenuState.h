#ifndef MenuState_H
#define MenuState_H

#include <vector>
#include <Ogre.h>
#include <OIS/OIS.h>

#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

#include "GameState.h"
#define NUM_NODOS 23

class MenuState : public Ogre::Singleton<MenuState>, public GameState{
    public:
       MenuState() {
            _render=true; 
        }

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        
        void createGUI();
        void createScene();
        void createOverlay();

        bool quit(const CEGUI::EventArgs &e);
        bool menuGUI(const CEGUI::EventArgs &e);
        bool creditsGUI(const CEGUI::EventArgs &e);

        bool recordsGUI(const CEGUI::EventArgs &e);
        bool lanzarJuego(const CEGUI::EventArgs &e);

        void ocultarBotones();
        void mostrarBotones();
        void attachButtons();

        CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

        /* Heredados de Ogre::Singleton. */
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;

        Ogre::Overlay *_ovAbout, *_ovRecords;

        Ogre::SceneNode  *_nodBase;

        float _timeSinceLastFrame;
        CEGUI::OgreRenderer* renderer;    
        CEGUI::Window* sheet;
        CEGUI::Window* quitButton;
        CEGUI::Window* creditsButton;
        CEGUI::Window* recordsButton;
        CEGUI::Window* gameButton;




        std::vector<Ogre::SceneNode*> _nodos;
        std::vector<Ogre::SceneNode*> _nodosRotar;
        std::vector<Ogre::AnimationState*> _animaciones;

        bool _exitGame;
        bool _render;

};

#endif
