#include "IntroState.h"
#include "MenuState.h"
#include "RecordManager.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter () {
  _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
  
  _camera = _sceneMgr->createCamera("MainCamera");
  _camera->setPosition(Ogre::Vector3(0, 3, 10));
  _camera->lookAt(Ogre::Vector3(0, 0, 0));
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(10000);

  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

  double width = _viewport->getActualWidth();
  double height = _viewport->getActualHeight();
  _camera->setAspectRatio(width / height);

  std::cout << "Cargando Records..." << std::endl;
  RecordManager::getSingletonPtr()->loadRecords();

  _pTrackManager=TrackManager::getSingletonPtr();
  _mainTrack=_pTrackManager->load("PlayTrack.mp3");


  createScene();
  createOverlay();
  _ovejas.reserve(4);
  _animaciones.reserve(4);

  _exitGame = false;
}

void IntroState::createScene() {
	Ogre::Entity *ent = NULL;
	Ogre::SceneNode *node = NULL;
	Ogre::AnimationState* animOveja;

	_camera->setPosition(Ogre::Vector3(10, 5, 85));
	_camera->lookAt(Ogre::Vector3(3, 0, 5));
	/* Cielo */
	_sceneMgr->setSkyDome(true, "Sky", 5, 8);

	/* Hierba */
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"GrassIntro", Ogre::Vector3(0, 1, 0));
	ent = _sceneMgr->createEntity("Grass.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	node->setVisible(false);

	Ogre::Plane plane1(Ogre::Vector3::UNIT_Y, 0);
	Ogre::MeshManager::getSingleton().createPlane("p1",
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
			500, 500, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
	node = _sceneMgr->createSceneNode("GroundIntro");
	Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEntIntro", "p1");
	groundEnt->setCastShadows(false);
	groundEnt->setMaterialName("Ground");
	node->attachObject(groundEnt);
	_sceneMgr->getRootSceneNode()->addChild(node);

	/*Arboles*/
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"ArbolesIntro", Ogre::Vector3(0, 0.001, 0));
	ent = _sceneMgr->createEntity("farm_top.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);

	/*Granja Izqda*/
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"Farm_leftIntro", Ogre::Vector3(0, 0.001, 0));
	ent = _sceneMgr->createEntity("farm_left.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);

	/*Ovejas*/
	for(int i=0; i<4; i++){
		node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
				"SheepIntro" + Ogre::StringConverter::toString(i), Ogre::Vector3(-30, 1.8, 30+(i*5)));
		ent = _sceneMgr->createEntity("Sheep.mesh");
		ent->setCastShadows(true);
		node->attachObject(ent);
		node->setVisible(true);
		//node->setScale(5,5,5);
		node->yaw(Ogre::Degree(180-(i*10)));
		_ovejas.push_back(node);

		/*Animacion oveja*/
		animOveja = ent->getAnimationState("Andar");
		animOveja->setEnabled(true);
		animOveja->setTimePosition(0);
		animOveja->setLoop(true);
		_animaciones.push_back(animOveja);
	}

	/*Lobo*/
	_lobo = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"LoboIntro" , Ogre::Vector3(-90, 2, 30));
	ent = _sceneMgr->createEntity("Wolf.mesh");
	ent->setCastShadows(true);
	_lobo->attachObject(ent);
	_lobo->setVisible(true);
	//_lobo->setScale(5,5,5);
	_lobo->yaw(Ogre::Degree(180));

	/*Animacion Lobo*/
	_animLobo = ent->getAnimationState("Correr");
	_animLobo->setEnabled(true);
	_animLobo->setTimePosition(0);
	_animLobo->setLoop(true);

	/* Sombras */
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	_sceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

	/* Iluminacion */
	node = _sceneMgr->createSceneNode("LightingNodeIntro");
	Ogre::Light *light = _sceneMgr->createLight("LightIntro");
	light->setType(Ogre::Light::LT_DIRECTIONAL);
	light->setDirection(Ogre::Vector3(0, -1, -1));
	node->attachObject(light);

	_mainTrack->play();
}

void IntroState::createOverlay () {
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _ovIntro = _overlayManager->getByName("Intro");
}//Fin createOverlay

void IntroState::exit () {
	_ovIntro->hide();
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause () {}

void IntroState::resume () {}

bool IntroState::frameStarted (const Ogre::FrameEvent& evt) {
	Ogre::Vector3 vn(-1, 0, 0);

	if(_time < 17.5){
		_time += evt.timeSinceLastFrame;
	} else if(!_ovIntro->isVisible()){
		_ovIntro->show();
	}

	for(int i=0; (unsigned) i < _animaciones.size(); i++){
		_animaciones[i]->addTime(evt.timeSinceLastFrame);
	}

	for(int i=0; (unsigned)i<_ovejas.size(); i++){
		_ovejas[i]->translate( _ovejas[i]->getOrientation() * vn * evt.timeSinceLastFrame * 5.0);
	}
	_animLobo->addTime(evt.timeSinceLastFrame);

	vn.x=1.5;
	_lobo->translate(vn * evt.timeSinceLastFrame * 5.0);
  return true;
}

bool IntroState::frameEnded (const Ogre::FrameEvent& evt){
  if (_exitGame){
    return false;
  }
  
  return true;
}

void IntroState::keyPressed (const OIS::KeyEvent &e){
  // Transición al siguiente estado.
  // Espacio --> PlayState
  if (e.key == OIS::KC_SPACE) {
  	if(_ovIntro->isVisible()){
    	changeState(MenuState::getSingletonPtr());
    } else {
    	_ovIntro->show();
    }
  }
}

void IntroState::keyReleased (const OIS::KeyEvent &e ){
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }
}

void IntroState::mouseMoved (const OIS::MouseEvent &e){}

void IntroState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id){}

void IntroState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id){}

IntroState* IntroState::getSingletonPtr (){
  return msSingleton;
}

IntroState& IntroState::getSingleton (){ 
  assert(msSingleton);
  return *msSingleton;
}
