#include "Sheep.h"

Sheep::Sheep(Ogre::SceneNode *node, Ogre::AnimationState *walk, 
	Ogre::AnimationState *stop, Ogre::AnimationState *jump,
	Ogre::AnimationState *dead):Animal(node){

	_animWalk = walk;
	_animStop = stop;
	_animJump = jump;
	_animDead = dead;
	_points = 0;
}

void Sheep::update(Ogre::Real deltaT){
	/* Actualizamos animacion */
	if(_state != -1){
		switch(_state){
			case REPOSO:
				_animStop->addTime(deltaT);
			break;
			case SALTAR:
				_animJump->addTime(deltaT);
			break;
			case ANDAR:
				_animWalk->addTime(deltaT);
			break;
			case MORIR:
				_animDead->addTime(deltaT);
				break;
		}
	}
	/* Actualizamos posicion */
	if(_node && _state == ANDAR){
		_node->translate(_dir * deltaT * VELOCIDAD);
	}//Fin if
}//Fin update

void Sheep::setAnimation(int state){
	Ogre::AnimationState *anim;

	/* Limpiamos animaciones */
	if(_state != -1){
		switch(_state){
			case REPOSO:
				anim = _animStop;
			break;
			case SALTAR:
				anim = _animJump;
			break;
			case ANDAR:
				anim = _animWalk;
			break;
			case MORIR:
				anim=_animDead;
				break;

		}//Fin switch

		anim->setEnabled(false);
	  	anim->setTimePosition(0);
	  	anim->setLoop(false);
  	}//Fin if

	_state = state;

	/* Activamos animaciones */
	switch(_state){
	case REPOSO:
		anim = _animStop;
		break;
	case SALTAR:
		anim = _animJump;
		break;
	case ANDAR:
		anim = _animWalk;
		break;
	case MORIR:
		anim = _animDead;
		break;
	}//Fin switch

	anim->setEnabled(true);
  	anim->setTimePosition(0);
  	anim->setLoop(true);
}//Fin setAnimation

void Sheep::restart(){
	_node->resetToInitialState();
	_node->setVisible(true);
	_points = 0;
	
	setAnimation(ANDAR);
}//Fin restart

void Sheep::finish(){
	Ogre::AnimationState *anim;

	/* Limpiamos animaciones */
	switch(_state){
		case REPOSO:
			anim = _animStop;
		break;
		case SALTAR:
			anim = _animJump;
		break;
		case ANDAR:
			anim = _animWalk;
		break;
		case MORIR:
			anim = _animDead;
			break;
	}

	anim->setEnabled(false);
  	anim->setTimePosition(0);
  	anim->setLoop(false);

  	/* Reiniciamos posicion */
	_node->resetToInitialState();
}

int Sheep::getState(){
	return _state;
}

int Sheep::getPoints(){
	return _points;
}

void Sheep::incPoints(){
	_points++;
}
