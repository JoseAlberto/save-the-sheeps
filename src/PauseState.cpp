#include "PauseState.h"
#include "PlayState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void
PauseState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("MainCamera");
  _viewport = _root->getAutoCreatedWindow()->getViewport(0);
  // Nuevo background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 0.0));

  _exitGame = false;

  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
  CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-10");

  CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");

  if(!_creado){
	  createGUI();
	  _creado=true;
  }

  createOverlay();


  CEGUI::MouseCursor::getSingleton().show();
  attachButtons();
}


void PauseState::createOverlay () {
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _ovPause = _overlayManager->getByName("Pause");
}//Fin createOverlay

void PauseState::createGUI(){
    /* Sheet */
    sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "SheetPause");


    /* New Game Button */
      _continueButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","_continueButton");
      _continueButton->setText("Continue");

      _continueButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
      _continueButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.45,0)));
      _continueButton->subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&PauseState::continueGame, this));


      /* Records Button */
      _backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","_backButton");
      _backButton->setText("Back");

      _backButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
      _backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.55,0)));
      _backButton->subscribeEvent(CEGUI::PushButton::EventClicked,
    		  CEGUI::Event::Subscriber(&PauseState::backMenu, this));

}//Fin createGUI

void
PauseState::exit ()
{

}

void
PauseState::pause ()
{
}

void
PauseState::resume ()
{
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
	CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);

  return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P) {
    popState();
  }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
	CEGUI::System::getSingleton().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
}

PauseState*
PauseState::getSingletonPtr ()
{
return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void PauseState::attachButtons(){
    /* Attaching buttons */

    sheet->addChildWindow(_continueButton);
    sheet->addChildWindow(_backButton);

    _continueButton->show();
    _backButton->show();
    _ovPause->show();
    CEGUI::System::getSingleton().setGUISheet(sheet);
}//Fin attachButtons

CEGUI::MouseButton PauseState::convertMouseButton(OIS::MouseButtonID id){
    CEGUI::MouseButton ceguiId;

    switch(id){
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }//Fin switch
  return ceguiId;
}//Fin convertMouseButton

bool PauseState::backMenu(const CEGUI::EventArgs &e){
	_continueButton->hide();
	_backButton->hide();
	_ovPause->hide();
	CEGUI::MouseCursor::getSingleton().hide();
	PlayState::getSingletonPtr()->finPartida();
	popState();
	return true;
}
 bool PauseState::continueGame(const CEGUI::EventArgs &e){

	 _continueButton->hide();
	 _backButton->hide();
	 _ovPause->hide();
	 CEGUI::MouseCursor::getSingleton().hide();
	 popState();
	 return true;
 }
