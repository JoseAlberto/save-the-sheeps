#include "Wolf.h"

Wolf::Wolf(Ogre::SceneNode *node, /*Ogre::SceneNode *box,*/ Ogre::AnimationState *walk,
	Ogre::AnimationState *stop, Ogre::AnimationState *run,
	Ogre::AnimationState *attack,Ogre::AnimationState *dead):Animal(node){

	_animWalk = walk;
	_animStop = stop;
	_animRun = run;
	_animAttack = attack;
	_animDead=dead;
	//_boxCollision = box;
	_hungry = true;

}

Wolf::~Wolf(){
	_ioPos.clear();
}

void Wolf::update(Ogre::Real deltaT){
	/* Actualizamos animacion */
	if(_state != -1){
		switch(_state){
			case REPOSO:
				_animStop->addTime(deltaT);
			break;
			case ANDAR:
				_animWalk->addTime(deltaT);
			break;
			case CORRER:
				_animRun->addTime(deltaT);
			break;
			case ATACAR:
				_animAttack->addTime(deltaT);
			break;
			case MORIR:
				_animDead->addTime(deltaT);
				break;
		}//Fin switch
	}//Fin if

	if(_state == ATACAR && _animAttack->hasEnded()){
		setAnimation(CORRER);
		_animAttack->setTimePosition(0.0);
		restart();
		_sheep->restart();
		_sheep->changeObjetive(_sheep->getObjetive());
	} else if(_state == MORIR && _animDead->hasEnded()){
		setRandomPosition();
		setAnimation(CORRER);
		_hungry = true;
		_animDead->setTimePosition(0.0);
	}//Fin if-else

	/* Actualizamos posicion */
	if(_node && _state == CORRER){
		_node->translate(_dir * deltaT * VELOCIDAD * 1.5);
	}

	/* Si ya no tiene hambre */
	if(!_hungry){
		for(int i = 0; (unsigned)i < _ioPos.size(); i++){
			/* Si ha llegado al punto destino */
			if(_node->getPosition().distance(_ioPos[i]) < 2.5){
				/* Reiniciamos lobo en una posicion aleatoria */
				setRandomPosition();
				_hungry = true;
				break;
			}//Fin if
		}//Fin for
	}//Fin if
}

void Wolf::setAnimation(int state){
	Ogre::AnimationState *anim;

	/* Limpiamos animaciones */
	if(_state != -1){
		switch(_state){
			case REPOSO:
				anim = _animStop;
			break;
			case ANDAR:
				anim = _animWalk;
			break;
			case CORRER:
				anim = _animRun;
			break;
			case ATACAR:
				anim = _animAttack;
			break;
			case MORIR:
				anim = _animDead;
				break;
		}

		anim->setEnabled(false);
	  	anim->setTimePosition(0);
	  	anim->setLoop(false);
  	}

	_state = state;

	/* Activamos animaciones */
	switch(_state){
		case REPOSO:
			anim = _animStop;
		break;
		case ANDAR:
			anim = _animWalk;
		break;
		case CORRER:
			anim = _animRun;
		break;
		case ATACAR:
			anim = _animAttack;
			anim->setLoop(false);
		break;
		case MORIR:
			anim = _animDead;
			anim->setLoop(false);
			break;
	}

	anim->setEnabled(true);
  	anim->setTimePosition(0);

  	if(_state != ATACAR && _state != MORIR){
  		anim->setLoop(true);
  	}
}

void Wolf::restart(){
	Ogre::Vector3 io = _ioPos[0];

	/* Elegimos el punto de salida mas cercano al lobo */
	for(int i = 1; (unsigned)i < _ioPos.size(); i++){
		if(_node->getPosition().distance(_ioPos[i]) < _node->getPosition(
			).distance(io)){
			io = _ioPos[i];
		}//Fin if
	}//Fin for

	/* Cambiamos por ese punto de salida el objetivo */
	changeObjetive(io);
	_hungry = false;
}

void Wolf::finish(){
	Ogre::AnimationState *anim;

	/* Limpiamos animaciones */
	switch(_state){
		case REPOSO:
			anim = _animStop;
		break;
		case ANDAR:
			anim = _animWalk;
		break;
		case CORRER:
			anim = _animRun;
		break;
		case ATACAR:
			anim = _animAttack;
		break;
		case MORIR:
			anim = _animDead;
			break;
	}

	anim->setEnabled(false);
  	anim->setTimePosition(0);
  	anim->setLoop(false);

  	/* Reiniciamos posicion */
  	_node->resetToInitialState();
}

bool Wolf::isHungry(){
	return _hungry;
}

void Wolf::addPositions(std::vector<Ogre::Vector3> v){
	_ioPos.reserve(v.size());

	for(int i = 0;(unsigned) i < v.size(); i++){
		_ioPos.push_back(v[i]);
	}//Fin for
}

void Wolf::setRandomPosition(){
	int idx = rand() % _ioPos.size();

	_node->setPosition(_ioPos[idx]);
}

Ogre::SceneNode * Wolf::getBox(){
	return _boxCollision;
}

int Wolf::getState(){
	return _state;
}

void Wolf::setSheep(Sheep *mySheep){
	_sheep=mySheep;
}
