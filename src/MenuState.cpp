#include "MenuState.h"
#include "PlayState.h"
#include "RecordManager.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream> 
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

void MenuState::enter (){
    _root = Ogre::Root::getSingletonPtr();
    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera); 
    /* Nuevo background colour. */
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

    _nodos.reserve(NUM_NODOS);
    _nodosRotar.reserve(4);
    _animaciones.reserve(5);

    if(_render){
    renderer = &CEGUI::OgreRenderer::bootstrapSystem();

    }

    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-10");

    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");


    if(_render){
    	createGUI();
    	_render=false;

    }

    createScene();
    createOverlay();

   	CEGUI::MouseCursor::getSingleton().show();
    attachButtons();

    mostrarBotones();


    _exitGame = false;

}//Fin enter

void MenuState::createScene(){
	/* Variables que vamos a usar */
	Ogre::Entity *ent = NULL;
	Ogre::SceneNode *node = NULL;
	Ogre::AnimationState* animOveja;

	_camera->setPosition(Ogre::Vector3(-10, 5, 85));
	_camera->lookAt(Ogre::Vector3(3, 0, 5));
	/* Cielo */
	_sceneMgr->setSkyDome(true, "Sky", 5, 8);

	Ogre::Plane plane1(Ogre::Vector3::UNIT_Y, 0);
	Ogre::MeshManager::getSingleton().createPlane("p1",
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
			500, 500, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
	node = _sceneMgr->createSceneNode("GroundMenu");
	Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEntMenu", "p1");
	groundEnt->setCastShadows(false);
	groundEnt->setMaterialName("Ground");
	node->attachObject(groundEnt);
	_nodos.push_back(node);
	_sceneMgr->getRootSceneNode()->addChild(node);

	/* Sombras */
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	_sceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

	/* Iluminacion */
	node = _sceneMgr->createSceneNode("LightingNodeMenu");
	Ogre::Light *light = _sceneMgr->createLight("LightMenu");
	light->setType(Ogre::Light::LT_DIRECTIONAL);
	light->setDirection(Ogre::Vector3(0, -1, -1));
	node->attachObject(light);
	_nodos.push_back(node);

	/* Platano */
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"PlatanoMenu", Ogre::Vector3(-0.5, 6, 60));
	ent = _sceneMgr->createEntity("Banana.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	node->setVisible(true);
	_nodos.push_back(node);
	_nodosRotar.push_back(node);

	/* Calabaza */
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"CalabazaMenu", Ogre::Vector3(-0.5, 5, 60));
	ent = _sceneMgr->createEntity("Calabaza.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	node->setVisible(true);
	_nodos.push_back(node);
	_nodosRotar.push_back(node);

	/* Tomato */
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"TomatoMenu", Ogre::Vector3(-0.5, 2.6, 60));
	ent = _sceneMgr->createEntity("tomato.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	node->setVisible(true);
	_nodos.push_back(node);
	_nodosRotar.push_back(node);

	/* Melon */
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"MelonMenu", Ogre::Vector3(-0.5, 0.3, 60));
	ent = _sceneMgr->createEntity("Watermelon.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	node->setVisible(true);
	_nodos.push_back(node);
	_nodosRotar.push_back(node);

	/*Granja*/
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"Farm_rightMenu", Ogre::Vector3(-20, 0.001, 50));
	ent = _sceneMgr->createEntity("farm_right.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	node->setVisible(true);
	node->yaw(Ogre::Degree(90));
	_nodos.push_back(node);

	/*Lobo*/
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"LoboIntro" , Ogre::Vector3(70, 2, 0));
	ent = _sceneMgr->createEntity("Wolf.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	node->setVisible(true);

	_nodos.push_back(node);
	animOveja = ent->getAnimationState("Reposo2");
	animOveja->setEnabled(true);
	animOveja->setTimePosition(0);
	animOveja->setLoop(true);
	_animaciones.push_back(animOveja);

	/*Ovejas*/
	for(int i=0; i<4; i++){
		node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
				"SheepMenu" + Ogre::StringConverter::toString(i), Ogre::Vector3(-5-(i*5), 1.8, 60));
		ent = _sceneMgr->createEntity("Sheep.mesh");
		ent->setCastShadows(true);
		node->attachObject(ent);
		node->setVisible(true);

		node->yaw(Ogre::Degree(150-(i*20)));
		_nodos.push_back(node);
		animOveja = ent->getAnimationState("Saltar");
		animOveja->setEnabled(true);
		animOveja->setTimePosition(0);
		animOveja->setLoop(true);
		_animaciones.push_back(animOveja);
	}

	int y=5; double x=0;
	/*Ovejas*/
	for(int i=4; i<14; i++){

		if(i==8){
			x=2.5;
			y=5;
		}

		node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
				"SheepMenu" + Ogre::StringConverter::toString(i), Ogre::Vector3(-15-(y*2), 1.8, -5+x));
		ent = _sceneMgr->createEntity("Sheep.mesh");
		ent->setCastShadows(true);
		node->attachObject(ent);
		node->setVisible(true);

		node->yaw(Ogre::Degree(130));
		_nodos.push_back(node);
		y++;
	}
	/*Arboles*/
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"Farm_topMenu", Ogre::Vector3(0, 0.001, 0));
	ent = _sceneMgr->createEntity("farm_top.mesh");
	ent->setCastShadows(true);
	node->attachObject(ent);
	_nodos.push_back(node);
}//Fin createScene

void MenuState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _ovAbout = _overlayManager->getByName("About");
    _ovRecords = _overlayManager->getByName("Records");
}//Fin createOverlay

void MenuState::exit (){
	ocultarBotones();
	CEGUI::MouseCursor::getSingleton().hide();

	_nodos.clear();
	_nodosRotar.clear();
	_animaciones.clear();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void MenuState::pause (){
    ocultarBotones();
    _sceneMgr->getSceneNode("MelonMenu")->setVisible(false);
    _sceneMgr->getSceneNode("CalabazaMenu")->setVisible(false);
    _sceneMgr->getSceneNode("TomatoMenu")->setVisible(false);
    _sceneMgr->getSceneNode("PlatanoMenu")->setVisible(false);
    CEGUI::MouseCursor::getSingleton().hide();
}//Fin pause

void MenuState::resume (){
    attachButtons();
    mostrarBotones();
    for(int i=0; (unsigned) i < _nodos.size(); i++){
    	_nodos[i]->setVisible(true);
   	}
    CEGUI::MouseCursor::getSingleton().show();
}//Fin resume

bool MenuState::frameStarted(const Ogre::FrameEvent& evt){
	if(_ovRecords->isVisible()){
		Ogre::OverlayElement *oe;
		oe = _overlayManager->getOverlayElement("RecordsText");
		oe->setCaption(RecordManager::getSingletonPtr()->toString());
	}

	CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
	for(int i=0; (unsigned) i < _nodosRotar.size(); i++){
		_nodosRotar[i]->yaw(Ogre::Degree(evt.timeSinceLastFrame*50));
	}

	for(int i=0; (unsigned) i < _animaciones.size(); i++){
		_animaciones[i]->addTime(evt.timeSinceLastFrame);
	}

    return true;
}//Fin frameStarted

bool MenuState::frameEnded(const Ogre::FrameEvent& evt){
	if(_exitGame){
	    std::cout << "Guardando Records..." << std::endl;
	    RecordManager::getSingletonPtr()->saveRecords();
	    return false;
	}//Fin if

    return true;
}//Fin frameEnded

void MenuState::keyPressed(const OIS::KeyEvent &e){
    switch(e.key){
        case OIS::KC_ESCAPE:
            _exitGame=true;
            break;
        default:
            /* Cualquier otra tecla no hace nada */
            break;
    }//Fin if
}//Fin keyPressed

void MenuState::keyReleased(const OIS::KeyEvent &e){}

void MenuState::mouseMoved(const OIS::MouseEvent &e){
	CEGUI::System::getSingleton().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}//Fin mouseMoved

void MenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
}//Fin mousePressed

void MenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
}//Fin mouseReleased

MenuState* MenuState::getSingletonPtr (){ 
    return msSingleton;
}//Fin getSingletonPtr

MenuState& MenuState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

void MenuState::createGUI(){
    /* Sheet */
    sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Sheet");


    /* New Game Button */
      gameButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","gameButton");
      gameButton->setText("New Game");

      gameButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
      gameButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.3,0)));
      gameButton->subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&MenuState::lanzarJuego, this));


      /* Records Button */
      recordsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","recordsButton");
      recordsButton->setText("Records");

      recordsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
      recordsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.4,0)));
      recordsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
    		  CEGUI::Event::Subscriber(&MenuState::recordsGUI, this));

    /* Credits button */
    creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","creditsButton");
    creditsButton->setText("About");

    creditsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.5,0)));
    creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::creditsGUI,this));


    /* Quit button */
    quitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","QuitButton");
    quitButton->setText("Exit");
    quitButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.6,0)));
    quitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::quit, this));




}//Fin createGUI

void MenuState::attachButtons(){
    /* Attaching buttons */

    sheet->addChildWindow(quitButton);
    sheet->addChildWindow(creditsButton);
    sheet->addChildWindow(recordsButton);
    sheet->addChildWindow(gameButton);

    CEGUI::System::getSingleton().setGUISheet(sheet);
}//Fin attachButtons

bool MenuState::quit(const CEGUI::EventArgs &e){
    _exitGame = true;
    return true;
}//Fin quit


bool MenuState::menuGUI(const CEGUI::EventArgs &e){
    /* Sheet */
    CEGUI::WindowManager::getSingleton().getWindow("Sheet");

    ocultarBotones();

    mostrarBotones();
    return true;
}//Fin menuGUI

bool MenuState::creditsGUI(const CEGUI::EventArgs &e){

	if(_ovRecords->isVisible()){
		_ovRecords->hide();
	}

	if(_ovAbout->isVisible()){
		_ovAbout->hide();
	} else {
		_ovAbout->show();
	}
    return true;
}//Fin creaditsGUI


bool MenuState::recordsGUI(const CEGUI::EventArgs &e){
    
	if(_ovAbout->isVisible()){
		_ovAbout->hide();
	}

	if(_ovRecords->isVisible()){
		_ovRecords->hide();
	} else {
		_ovRecords->show();
	}
   
    return true;
}//Fin recordsGUI

bool MenuState::lanzarJuego(const CEGUI::EventArgs &e){

	for(int i=0; (unsigned) i < _nodos.size(); i++){
			_nodos[i]->setVisible(false);
	}
    changeState(PlayState::getSingletonPtr());

	return true;
}//Fin lanzarJuego

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id){
    CEGUI::MouseButton ceguiId;

    switch(id){
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }//Fin switch
  return ceguiId;
}//Fin convertMouseButton

void MenuState::ocultarBotones(){
    /* Attaching buttons */
	_ovRecords->hide();
	_ovAbout->hide();
    quitButton->hide();
    creditsButton->hide();
    recordsButton->hide();
    gameButton->hide();

}//Fin ocultarBotones

void MenuState::mostrarBotones(){
    quitButton->show();
    creditsButton->show();
    recordsButton->show();
    gameButton->show();
}//Fin mostrarBotones
