#include "AnimalsManager.h"


template<> AnimalsManager* Ogre::Singleton<AnimalsManager>::msSingleton = 0;

AnimalsManager::AnimalsManager(){
	/* Reiniciamos la semilla */
  	srand(time(NULL));
  	_sheepEnd = Ogre::Vector3(-50, 2.201, 5);
}

void AnimalsManager::clear(){
	_wBodies.clear();  _wShapes.clear();
	_sheeps.clear();  _wolves.clear();
	_grass.clear();  _time.clear();
	_grassPos.clear();  _wolvesPos.clear();
}

void AnimalsManager::loadConfig(){
	std::string str;
	Ogre::StringVector vct;
	/* Lectura del fichero save_the_sheeps.conf */
	std::ifstream file("save_the_sheeps.conf");

	if (file.is_open()){
		getline(file, str);
		for(int i = 0; i < 3; i++){
			getline(file, str);
			/* Hacemos split por " " para obtener las coordenadas */
        	vct = Ogre::StringUtil::split(str.c_str(), " ");
        	/* Agregamos el vector */
        	_grassPos.push_back(Ogre::Vector3(Ogre::StringConverter::parseLong(vct[0]),
        									Ogre::StringConverter::parseLong(vct[1]),
        									Ogre::StringConverter::parseLong(vct[2])));
		}
        getline(file, str);
		for(int i = 0; i < 3; i++){
			getline(file, str);
			/* Hacemos split por " " para obtener las coordenadas */
        	vct = Ogre::StringUtil::split(str.c_str(), " ");
        	/* Agregamos el vector */
        	_wolvesPos.push_back(Ogre::Vector3(Ogre::StringConverter::parseLong(vct[0]),
        									Ogre::StringConverter::parseLong(vct[1]),
        									Ogre::StringConverter::parseLong(vct[2])));
		}
        file.close();
	}

	/* Agregacion de las posiciones a los lobos */
	for(int i = 0; i < NUM_ELEMENTS; i++){
		_wolves[i]->addPositions(_wolvesPos);
	}//Fin for
}

void AnimalsManager::init(Ogre::SceneManager* mgr, OgreBulletDynamics::DynamicsWorld* world){
	_sceneMgr = mgr;
	_world = world;

	_sheeps.reserve(NUM_ELEMENTS);
	_wolves.reserve(NUM_ELEMENTS);
	_grass.reserve(NUM_ELEMENTS);
	_time.reserve(NUM_ELEMENTS);

	_sSheeps = _dSheeps = 0;

	_pSoundFXManager = SoundFXManager::getSingletonPtr();
	_grunir=_pSoundFXManager->load("gruñir.ogg");
	_salvada=_pSoundFXManager->load("savedsheep.ogg");
	_balar=_pSoundFXManager->load("oveja.ogg");
}

void AnimalsManager::createAnimals(){
	Ogre::Entity *ent;
	Ogre::SceneNode *node;
	//Ogre::SceneNode *nodeBox;
	Ogre::AnimationState *animW, *animS, *animJ, *animA, *animR, *animM;

	Sheep *sheep;
	Wolf *wolf;

	for(int i = 0; i < NUM_ELEMENTS; i++){
		/* Tiempo */
		_time[i] = 0.0;

		/* Hierba */
		node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
	    	"Grass" + Ogre::StringConverter::toString(i), Ogre::Vector3(0, 2.201, 0));
	  	ent = _sceneMgr->createEntity("Grass.mesh");
	  	ent->setCastShadows(true);
	  	node->attachObject(ent);
	  	node->setVisible(false);
	  	node->setInitialState();

	  	_grass.push_back(node);

	  	/* Oveja */
	  	node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    	"Sheep" + Ogre::StringConverter::toString(i), Ogre::Vector3(45, 1.801, -4));
  		ent = _sceneMgr->createEntity("Sheep.mesh");
  		ent->setCastShadows(true);
  		node->attachObject(ent);
  		node->setVisible(false);
  		node->setInitialState();

  		animW = ent->getAnimationState("Andar");
  		animW->setEnabled(true);
  		animW->setTimePosition(0);
  		animW->setLoop(true);
  		animS = ent->getAnimationState("Reposo");
  		animS->setEnabled(false);
  		animS->setTimePosition(0);
  		animS->setLoop(false);
  		animJ = ent->getAnimationState("Saltar");
  		animJ->setEnabled(false);
  		animJ->setTimePosition(0);
  		animJ->setLoop(false);
  		animM = ent->getAnimationState("Morir");
  		animM->setEnabled(false);
  		animM->setTimePosition(0);
  		animM->setLoop(false);

  		sheep = new Sheep(node, animW, animS, animJ, animM);

  		_sheeps.push_back(sheep);

  		/* Lobo */
  		 node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
  				"Wolf" + Ogre::StringConverter::toString(i));
  		ent = _sceneMgr->createEntity("Wolf.mesh");
  		node->attachObject(ent);
  		node->setVisible(false);
  		node->setInitialState();

  		animW = ent->getAnimationState("Andar");
  		animW->setEnabled(true);
  		animW->setTimePosition(0);
  		animW->setLoop(true);
  		///////////////////////////////////////////////
  		animS = NULL;
  		///////////////////////////////////////////////
  		animR = ent->getAnimationState("Correr");
  		animR->setEnabled(false);
  		animR->setTimePosition(0);
  		animR->setLoop(false);
  		animA = ent->getAnimationState("Atacar");
  		animA->setEnabled(false);
  		animA->setTimePosition(0);
  		animA->setLoop(false);
  		animM = ent->getAnimationState("Morir");
  		animM->setEnabled(false);
  		animM->setTimePosition(0);
  		animM->setLoop(false);

  		wolf = new Wolf(node, animW, animS, animR, animA, animM);

  		/*Creamos los objetos para Ogrebullet*/
  		OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = NULL;
  		OgreBulletCollisions::CollisionShape *bodyShape = NULL;
  		OgreBulletDynamics::RigidBody *rigidBody = NULL;

  		Ogre::Vector3 size = Ogre::Vector3::ZERO;
  		Ogre::Vector3 position(-30, 0.001, 43);
  		Ogre::AxisAlignedBox boundingB = ent->getBoundingBox();

  		size = boundingB.getSize();
  		size /= 2.5f;   // El tamano en Bullet se indica desde el centro
  		bodyShape = new OgreBulletCollisions::BoxCollisionShape(size);

  		rigidBody = new OgreBulletDynamics::RigidBody("wolf" +
  		    Ogre::StringConverter::toString(i), _world);

  		rigidBody->setShape(node, bodyShape,
  		  	0.1 /* Restitucion */, 0.3/* Friccion */,
  		  	2.0 /* Masa */, position /* Posicion inicial */,
  		 	Ogre::Quaternion::IDENTITY /* Orientacion */);

  		_wShapes.push_back(bodyShape);   _wBodies.push_back(rigidBody);

  		_wolves.push_back(wolf);
	}//Fin for
	
	loadConfig();
}//Fin createAnimals

void AnimalsManager::update(Ogre::Real deltaT){
	Sheep *sheep;

	if(_run){
		/* Actualizacion de animaciones oveja/lobo y objetivo lobo */
		for(int i = 0; i < _level; i++){
			_sheeps[i]->update(deltaT);

			if(_wolves[i]->isHungry()){
				sheep = _sheeps[0];
				/* Calculo de la oveja mas cercana para el lobo i */
				for(int j = 1; j < _level; j++){
					if(_wolves[i]->getNode()->getPosition().distance(
						_sheeps[j]->getNode()->getPosition()) < _wolves[i]->getNode(
						)->getPosition().distance(sheep->getNode()->getPosition())){
						sheep = _sheeps[j];
					}//Fin if
				}//Fin for

				_wolves[i]->changeObjetive(sheep->getNode()->getPosition());
			}//Fin if

			_wolves[i]->update(deltaT);
		}//Fin for

		/* Deteccion de colisiones de ovejas con lobos */
		bool flag;
		for(int i = 0; i < _level; i++){
			flag = false;
			/* Si lobo i tiene hambre y esta cerca de una oveja, se la come */
			if(_wolves[i]->isHungry()){
				for(int j = 0; j < _level && !flag; j++){
					if((_wolves[i]->getNode()->getPosition().distance(_sheeps[j]->getNode()->getPosition()) < 3)
							&&_wolves[i]->getState() != ATACAR){
						/*Lanzamos los sonidos*/
						_grunir->play(false);
						_balar->play(false);
						/* Pongo la oveja en reposo */
						_sheeps[j]->setAnimation(MORIR);
						/*Le paso la oveja al lobo para que la reinicie cuando termine atacar */
						_wolves[i]->setSheep(_sheeps[j]);
						/* Movimiento del lobo a punto de salida */
						_wolves[i]->setAnimation(ATACAR);
						/* Incrementamos la variable de las ovejas que la han palmao */
						_dSheeps++;
						/* Salimos del bucle */
						flag = true;
	    			}//Fin if
	    		}//Fin for
    		}//Fin if
		}//Fin for

		/* Deteccion de colisiones de ovejas con hierba */
		for(int i = 0; i < _level; i++){
			/* Si se da el caso de que la oveja ya esta junto a un poco de hierba */
  			if(_sheeps[i]->getNode()->getPosition().distance(_grass[i]->getPosition()) < 1.5){
    			_time[i] += deltaT;

    			if(_sheeps[i]->getState() == ANDAR){
      				_sheeps[i]->setAnimation(SALTAR);
    			} else if(_time[i] >= GRASS_TIME){
    				/* Incrementamos puntuacion oveja */
      				_sheeps[i]->incPoints();

      				/* Movemos hierba */
      				_time[i] = 0.0;
      				_grass[i]->setPosition((rand() % 50) - 25, 
      										_grass[i]->getPosition().y,
      										(rand() % 50) - 25);

      				/* Si la oveja ya ha comido todo lo que tenia que comer */
      				if(_sheeps[i]->getPoints() == (_level + 2)){
      					/* Cambiamos el objetivo de la oveja por el del granero destino */
      					if(_sheeps[i]->getObjetive() != _sheepEnd){
      						_sheeps[i]->setAnimation(ANDAR);
      						_sheeps[i]->changeObjetive(_sheepEnd);
      					}
      				} else {
      					_sheeps[i]->setAnimation(ANDAR);
      					_sheeps[i]->changeObjetive(_grass[i]->getPosition());
      				}//Fin if-else
      			}//Fin if-else
    		}//Fin if

    		/* Si la oveja a llegado al granero destino, la reiniciamos e incrementamos puntos */
    		if(_sheeps[i]->getNode()->getPosition().distance(_sheepEnd) < 1){
    			_salvada->play(false);
      			_sSheeps++;
      			_sheeps[i]->restart();
      			_sheeps[i]->changeObjetive(_grass[i]->getPosition());
      		}//Fin if-else
    	}//Fin for
	}//Fin if
	updateBodies();
}//Fin update

void AnimalsManager::start(int level){
	_level = level;

	/* Reiniciamos escenario en funcion del nivel */
	for(int i = 0; i < _level; i++){
		/* Reinicio de tiempo */
		_time[i] = 0.0;

		/* Reinicio de hierba */
		_grass[i]->setPosition(_grassPos[i]);
		_grass[i]->setVisible(true);

		/* Reinicio de oveja */
		_sheeps[i]->setAnimation(ANDAR);
		_sheeps[i]->changeObjetive(_grass[i]->getPosition());
		_sheeps[i]->getNode()->setVisible(true);

		/* Reinicio de lobo */
		_wolves[i]->setRandomPosition();
		_wolves[i]->setAnimation(CORRER);
		_wolves[i]->getNode()->setVisible(true);
		/* El objetivo al lobo siempre se le da antes de actualizarlo,
		   no es necesario darselo aqui */
	}//Fin for

	_run = true;
}//Fin start

void AnimalsManager::stop(){
	_run = false;

	for(int i = 0; i < _level; i++){
		/* Reinicio de oveja, lobo, hierba y tiempo */
		_sheeps[i]->finish();
		_wolves[i]->finish();
		_grass[i]->resetToInitialState();
		_time[i] = 0.0;
	}//Fin for

	/* Reinicio de variables de ovejas vivas y eso */
	_sSheeps = _dSheeps = 0;
}//Fin stop

int AnimalsManager::getSaveSheeps(){
	return _sSheeps;
}

void AnimalsManager::setSaveSheeps(int saved){
	_sSheeps=saved;
}
int AnimalsManager::getDieSheeps(){
	return _dSheeps;
}

AnimalsManager* AnimalsManager::getSingletonPtr(){
  return msSingleton;
}

AnimalsManager& AnimalsManager::getSingleton(){ 
  assert(msSingleton);
  return *msSingleton;
}

void AnimalsManager::updateBodies(){
	for(int i=0;(unsigned) i < _wBodies.size(); i++){
		Ogre::Vector3 aux(1,0,1);
		Ogre::Vector3 position(_wolves[i]->getNode()->getPosition()); //Have to be initialized
		btTransform transform; //Declaration of the btTransform
		transform.setIdentity(); //This function put the variable of the object to default. The ctor of btTransform doesnt do it.
		transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(position)); //Set the new position/origin
		_wBodies[i]->getBulletRigidBody()->setWorldTransform(transform); //Apply the btTransform to the body
	}
}

void AnimalsManager::restartWolf(int wolf){
	//_wolves[wolf]->getNode()->resetToInitialState();
	_wolves[wolf]->setAnimation(MORIR);
}
