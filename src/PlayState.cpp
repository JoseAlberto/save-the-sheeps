#include "PlayState.h"
#include "PauseState.h"
#include "MenuState.h"
#include "EndState.h"
#include <string>

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void PlayState::enter () {
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("MainCamera");
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  _pSoundFXManager = SoundFXManager::getSingletonPtr();
  _pTrackManager = TrackManager::getSingletonPtr();

  _level = 1;
  _tiempo = 0.0;
  _numEntities = 0;
  _fuerza = MIN_FUERZA;
  _score = _savedSheeps = 0;

  _exitGame= _endGame = false;
  _sPress = _uPress = _dPress = _rPress = _lPress = false;

  _o_power = new Ogre::OverlayElement*[5];

  /*Cargamos los sonidos*/
  //_disparo, _llorar, _gruñir, _salvada, _balar
  _disparo = _pSoundFXManager->load("disparo.ogg");
  _llorar=_pSoundFXManager->load("lloro.ogg");

  /* Creamos la escena */
  createScene();
  /* Creamos los overlays */
  createOverlay();
  /* Creacion de los elementos iniciales del mundo*/
  createInitialWorld();

  /* Lanzamos el animalsManager */
  AnimalsManager::getSingletonPtr()->init(_sceneMgr, _world);
  AnimalsManager::getSingletonPtr()->createAnimals();
  AnimalsManager::getSingletonPtr()->start(_level);
}

void PlayState::createScene() {
  Ogre::Entity *ent = NULL;
  Ogre::SceneNode *node = NULL;

  _camera->setPosition(Ogre::Vector3(-10, 25, 80));
  _camera->lookAt(Ogre::Vector3(3, 0, 5));

  /* Cielo */
  _sceneMgr->setSkyDome(true, "Sky", 5, 8);

  /* Cáñon */
  _nodBase = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    "Base", Ogre::Vector3(10, 0.001, 45));
  ent = _sceneMgr->createEntity("Cannion_Base.mesh");
  _nodBase->attachObject(ent);

  _nodCannion = _nodBase->createChildSceneNode("Cannion", 
    Ogre::Vector3(0, 1, 0));
  ent = _sceneMgr->createEntity("Cannion.mesh");
  _nodCannion->attachObject(ent);
  _animCannion = ent->getAnimationState("Disparar");
  _animCannion->setEnabled(false);
  _animCannion->setTimePosition(0);
  _animCannion->setLoop(false);

  _smoke = _sceneMgr->createParticleSystem("Ps","flame");
  _smoke->getEmitter(0)->setEnabled(false);
  _nodCannion->attachObject(_smoke);

  _nodShoot = _nodCannion->createChildSceneNode("Disparo",
    Ogre::Vector3(0, 1.7, -2));

  /* Granja */
  node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    "Farm_top", Ogre::Vector3(0, -0.5, 0));
  ent = _sceneMgr->createEntity("farm_top.mesh");
  ent->setCastShadows(true);
  node->attachObject(ent);

  node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    "Farm_left", Ogre::Vector3(0, 0.001, 0));
  ent = _sceneMgr->createEntity("farm_left.mesh");
  ent->setCastShadows(true);
  node->attachObject(ent);

  node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    "Farm_bot", Ogre::Vector3(0, 0.001, 5));
  ent = _sceneMgr->createEntity("farm_bot.mesh");
  ent->setCastShadows(true);
  node->attachObject(ent);

  node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    "Farm_right", Ogre::Vector3(0, 0.001, 0));
  ent = _sceneMgr->createEntity("farm_right.mesh");
  ent->setCastShadows(true);
  node->attachObject(ent);

  /* Fisica */
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);  
  node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
    "debugNode", Ogre::Vector3::ZERO);
  node->attachObject(static_cast<Ogre::SimpleRenderable *>(_debugDrawer));

  Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
    Ogre::Vector3 (-10000, -10000, -10000), 
    Ogre::Vector3 (10000,  10000,  10000));
  Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
     worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);
  _world->setShowDebugShapes (false);  // Muestra los collision shapes

  /* Sombras */
  _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
  _sceneMgr->setAmbientLight(Ogre::ColourValue(0.75, 0.75, 0.75));

  /* Iluminacion */
  node = _sceneMgr->createSceneNode("LightingNode");
  Ogre::Light *light = _sceneMgr->createLight("Light");
  light->setType(Ogre::Light::LT_DIRECTIONAL);
  light->setDirection(Ogre::Vector3(-0.5, -1, -1));
  node->attachObject(light);
}

void PlayState::createOverlay () {
    std::stringstream st;

    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _ovScore = _overlayManager->getByName("Score");
    _o_score = _overlayManager->getOverlayElement("PlayTextWolf");
    _o_sheeps = _overlayManager->getOverlayElement("PlayTextSheep");
    _ovScore->show();

    /* Power */
    _ovPower = _overlayManager->getByName("Power");
    for(int i = 0; i < 5; i++){
        st << "imgPower" << (i + 1) << "_5";
        _o_power[i] = _overlayManager->getOverlayElement(st.str());
        st.str("");
    }//Fin for

    _o_power[0]->show();
    _o_power[1]->hide();
    _o_power[2]->hide();
    _o_power[3]->hide();
    _o_power[4]->hide();
    _ovPower->show();
}//Fin createOverlay

void PlayState::updateText(){
    std::stringstream sc;

    if(_score < 100){
        sc << "0";
        if(_score < 10){
            sc << "0";
        }//Fin if
    }//Fin if

    sc << _score;

    _o_score->setCaption(sc.str());

    sc.str(""); sc << _savedSheeps << "/" << _level * 2;
    _o_sheeps->setCaption(sc.str());
}//Fin actualizarScore

void PlayState::createInitialWorld(){
  /* Creacion de la entidad y del SceneNode */
  Ogre::Plane plane1(Ogre::Vector3::UNIT_Y, 0);
  Ogre::MeshManager::getSingleton().createPlane("p1",
  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
  500, 500, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
  Ogre::SceneNode* node = _sceneMgr->createSceneNode("Ground");
  Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "p1");
  groundEnt->setCastShadows(false);
  groundEnt->setMaterialName("Ground");
  node->attachObject(groundEnt);
  _sceneMgr->getRootSceneNode()->addChild(node);

  /* Creamos forma de colision para el plano */ 
  OgreBulletCollisions::CollisionShape *Shape;
  Shape = new OgreBulletCollisions::StaticPlaneCollisionShape
    (Ogre::Vector3::UNIT_Y, 0);
  OgreBulletDynamics::RigidBody *rigidBodyPlane = new 
    OgreBulletDynamics::RigidBody("rigidBodyPlane", _world);

  /* Creamos la forma estatica (forma, Restitucion, Friccion) */
  rigidBodyPlane->setStaticShape(Shape, 0.1, 0.8); 
  
  /* Anadimos los objetos Shape y RigidBody */
  _shapes.push_back(Shape);      _bodies.push_back(rigidBodyPlane);
}

void PlayState::shootVegetable () {
  Ogre::Vector3 pos = Ogre::Vector3::ZERO;
  
  pos = _nodShoot->_getDerivedPosition();
  
  int tipo = (unsigned)rand() % NUM_VEGETABLES;
  Ogre::Entity *entity = NULL;
  switch(tipo){
    case 0:
      entity = _sceneMgr->createEntity("Calabaza" +
        Ogre::StringConverter::toString(_numEntities), "Calabaza.mesh");
      break;
    case 1:
      entity = _sceneMgr->createEntity("Tomate" +
        Ogre::StringConverter::toString(_numEntities), "tomato.mesh");
      break;
  }

  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->
    createChildSceneNode();
  node->attachObject(entity);

  OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = NULL; 
  OgreBulletCollisions::CollisionShape *bodyShape = NULL;
  OgreBulletDynamics::RigidBody *rigidBody = NULL;

  trimeshConverter = new 
    OgreBulletCollisions::StaticMeshToShapeConverter(entity);
  bodyShape = trimeshConverter->createConvex();
  delete trimeshConverter;

  rigidBody = new OgreBulletDynamics::RigidBody("rigidBody" + 
    Ogre::StringConverter::toString(_numEntities), _world);

  rigidBody->setShape(node, bodyShape,
         0.6 /* Restitucion */, 0.6 /* Friccion */,
         5.0 /* Masa */, pos /* Posicion inicial */,
         Ogre::Quaternion::IDENTITY /* Orientacion */);

  rigidBody->setLinearVelocity(_dir * _fuerza);

  _numEntities++;

  _shapes.push_back(bodyShape);   _bodies.push_back(rigidBody);
}

void PlayState::exit () {
  /* Eliminar cuerpos rigidos */
  std::deque <OgreBulletDynamics::RigidBody *>::iterator 
     itBody = _bodies.begin();
  while (_bodies.end() != itBody) {   
    delete *itBody;  ++itBody;
  }//Fin while
 
  /* Eliminar formas de colision */
  std::deque<OgreBulletCollisions::CollisionShape *>::iterator 
    itShape = _shapes.begin();
  while (_shapes.end() != itShape) {   
    delete *itShape; ++itShape;
  }//Fin while

  _bodies.clear();  _shapes.clear();

  /* Eliminar mundo dinamico y debugDrawer */
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;

  /* Limpiamos AnimalsManager */
  AnimalsManager::getSingletonPtr()->clear();

  _ovPower->hide();
  delete [] _o_power;

  _ovScore->hide();
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void PlayState::pause () {}

void PlayState::resume () {
	if(_endGame){
	 changeState(MenuState::getSingletonPtr());
	}//Fin if
}

bool PlayState::frameStarted (const Ogre::FrameEvent& evt) {
  _deltaT = evt.timeSinceLastFrame;

  if(!_endGame){
    /* Movimiento del cañon */
    if(_rPress && _nodBase->getOrientation().zAxis().x > (-0.95)){
      _nodBase->yaw(Ogre::Degree(-180 * _deltaT));
    } else if(_lPress && _nodBase->getOrientation().zAxis().x < (0.95)){
      _nodBase->yaw(Ogre::Degree(180 * _deltaT));
    } else if(_uPress && _nodCannion->getOrientation().zAxis().y > (-0.5)){
      _nodCannion->pitch(Ogre::Degree(180 * _deltaT));
    } else if(_dPress && _nodCannion->getOrientation().zAxis().y < (0.15)){
      _nodCannion->pitch(Ogre::Degree((-180) * _deltaT));
    }

    /* Animacion del cañon */
    if (_animCannion->getEnabled() && !_animCannion->hasEnded()){
      _animCannion->addTime(evt.timeSinceLastFrame);
    }//Fin if

    /* Calculo de la direccion del disparo */
    _dir = _nodBase->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
    _dir.y = (_nodCannion->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z).y;

    /* Incremento de la fuerza de disparo */
    if(_sPress && (_fuerza <= MAX_FUERZA)){
      _fuerza += INC_FUERZA;
    }
    updateOvPower();

    /* Simulacion del mundo */
    _world->stepSimulation(_deltaT);
    /* Actualizacion de animales */
    AnimalsManager::getSingletonPtr()->update(_deltaT);

    /* Deteccion de colisiones */
    DetectCollisionDrain();
    /* Borrado de elementos dormidos */
    deleteSlept();

    _savedSheeps = AnimalsManager::getSingletonPtr()->getSaveSheeps();

    if(_savedSheeps == (_level * 2)){
      AnimalsManager::getSingletonPtr()->stop();
    
      /* Mostramos overlay de nivel */
      ++_level;
      /* Comprobamos el nivel actual */
      if(_level <= MAX_LEVEL){
        /* Overlay cambio de nivel */
        AnimalsManager::getSingletonPtr()->start(_level);
      } else {
        _endGame = true;
        /* Pasamos al EndState la puntuacion*/
        EndState::getSingletonPtr()->setScore(_score);
      }//Fin if-else
    }//Fin if
  }//fin if endgame

  updateText();

  return true;
}//Fin frameStarted

bool PlayState::frameEnded (const Ogre::FrameEvent& evt) {
  _deltaT = evt.timeSinceLastFrame;
  
  /* Segunda actualizacion del mundo */
  _world->stepSimulation(_deltaT);

  /*Si endGrame cambiamos al EndState*/
  if(_endGame){
	  AnimalsManager::getSingletonPtr()->stop();
	  AnimalsManager::getSingletonPtr()->clear();
	  pushState(EndState::getSingletonPtr());
  }

  if (_exitGame){
    changeState(MenuState::getSingletonPtr());
  }
  
  return true;
}

void PlayState::updateOvPower(){
  int aux = 0;

  if(_fuerza >= 10.0 && _fuerza < 20.0){
    aux = 1;
  } else if(_fuerza >= 20.0 && _fuerza < 30.0){
    aux = 2;
  } else if(_fuerza >= 30.0 && _fuerza < 40.0){
    aux = 3;
  } else {
    aux = 4;
  }

  switch(aux){
    case 0:
      _o_power[0]->show();
      _o_power[1]->hide();
      _o_power[2]->hide();
      _o_power[3]->hide();
      _o_power[4]->hide();
      break;
    case 1:
      _o_power[0]->hide();
      _o_power[1]->show();
      _o_power[2]->hide();
      _o_power[3]->hide();
      _o_power[4]->hide();
      break;
    case 2:
      _o_power[0]->hide();
      _o_power[1]->hide();
      _o_power[2]->show();
      _o_power[3]->hide();
      _o_power[4]->hide();
      break;
    case 3:
      _o_power[0]->hide();
      _o_power[1]->hide();
      _o_power[2]->hide();
      _o_power[3]->show();
      _o_power[4]->hide();
      break;
    case 4:
      _o_power[0]->hide();
      _o_power[1]->hide();
      _o_power[2]->hide();
      _o_power[3]->hide();
      _o_power[4]->show();
      break;
  }

  std::cout << aux << ", " << _fuerza << std::endl;
}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
	switch(e.key){
	case OIS::KC_RIGHT:
		_rPress = true;
		break;
	case OIS::KC_LEFT:
		_lPress = true;
		break;
	case OIS::KC_UP:
		_uPress = true;
		break;
	case OIS::KC_DOWN:
		_dPress = true;
		break;
	case OIS::KC_SPACE:
		_sPress = true;
		break;
	case OIS::KC_J:
		AnimalsManager::getSingletonPtr()->setSaveSheeps(_level * 2);
		break;
	case OIS::KC_P:
		pushState(PauseState::getSingletonPtr());
		break;
	default:
		break;
	}
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
  switch(e.key){
    case OIS::KC_ESCAPE:
      _exitGame = true;
      break;
    case OIS::KC_RIGHT:
      _rPress = false;
      break;
    case OIS::KC_LEFT:
      _lPress = false;
      break;
    case OIS::KC_UP:
      _uPress = false;
      break;
    case OIS::KC_DOWN:
      _dPress = false;
      break;
    case OIS::KC_SPACE:
      _sPress = false;
      _animCannion->setTimePosition(0.0);
      _animCannion->setEnabled(true);
      _animCannion->setLoop(false);
      shootVegetable();
      _fuerza = MIN_FUERZA;
      _smoke->getEmitter(0)->setEnabled(true);
      _disparo->play(false);
      //_smoke->getEmitter(0)->setDuration(1.0);
      break;
    default:
      break;
  } 
}

void PlayState::mouseMoved (const OIS::MouseEvent &e) {}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

PlayState* PlayState::getSingletonPtr () {
  return msSingleton;
}

PlayState& PlayState::getSingleton () { 
  assert(msSingleton);
  return *msSingleton;
}

void PlayState::DetectCollisionDrain() {

	  btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
	  int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

	  for (int i=0;i<numManifolds;i++) {
	    btPersistentManifold* contactManifold =
	      bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
	    btCollisionObject* obA =
	      const_cast<btCollisionObject*>(contactManifold->getBody0());
	    btCollisionObject* obB =
	      const_cast<btCollisionObject*>(contactManifold->getBody1());

	    Ogre::SceneNode* wolf0 = _sceneMgr->getSceneNode("Wolf0");
	    Ogre::SceneNode* wolf1 = _sceneMgr->getSceneNode("Wolf1");
	    Ogre::SceneNode* wolf2 = _sceneMgr->getSceneNode("Wolf2");
	    Ogre::SceneNode* ground = _sceneMgr->getSceneNode("Ground");

	    OgreBulletCollisions::Object *obGround = _world->findObject(ground);

	    OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
	    OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);

	    Ogre::SceneNode* node = NULL;
	    Ogre::SceneNode* wolf = NULL;

	    /* Se comprueba para cada lobo */
	    for(int i=0; i < _level ; i++){
	    	wolf = _sceneMgr->getSceneNode("Wolf" + Ogre::StringConverter::toString(i));
	    	OgreBulletCollisions::Object *obDrain = _world->findObject(wolf);

	    	/* Se evitan las colisiones con el suelo */
	    	if((obOB_A != obGround) && (obOB_B != obGround)){

	    		if ((obOB_A == obDrain) || (obOB_B == obDrain)) {

	    			if ((obOB_A != obDrain) && (obOB_A)) {
	    				node = obOB_A->getRootNode(); //delete obOB_A;
	    				obOB_A->getBulletObject()->forceActivationState(/*ISLAND_SLEEPING*/ 2);


	    			} else if ((obOB_B != obDrain) && (obOB_B)) {
	    				node = obOB_B->getRootNode(); //delete obOB_B;
	    				obOB_B->getBulletObject()->forceActivationState(/*ISLAND_SLEEPING*/ 2);
	    			}//Fin if-else

	    			if (node) {
	    				//si no ha chocado con otro lobo
	    				if(node != wolf1 && node != wolf2 && node != wolf0){
	    					_llorar->play(false);
	    					AnimalsManager::getSingletonPtr()->restartWolf(i);
	    					_score+=1;
	    					//obDrain->getRootNode()->resetToInitialState();
	    				}//Fin if
	    			}//Fin if
	    		}//Fin if
	    	}//Fin si es suelo
	    }//Fin for, comprobaciones por lobo
	  }//Fin for numManifolds
}//Fin DetectCollisionDrain

/*Busca los objetos que están dormidos en la deque _bodies
 * Si hay alguno dormido lo elimina. Hay que tener en cuenta
 * que cuando se hace erase el puntero avanza, por lo que si volvemos a
 * incrementar saltamos un elemento o si nos encontramos en el último
 * tenemos errores. Por eso hay que decrementarlo.*/
void PlayState::deleteSlept(){
	 Ogre::SceneNode *nodo;
	 OgreBulletDynamics::RigidBody *aux;

	 for(std::deque<OgreBulletDynamics::RigidBody *>::iterator it= _bodies.begin();
			 it !=_bodies.end(); it++){
		 	 aux=*it;
		 if(!(aux->getBulletRigidBody()->isActive())){
			 nodo=aux->getSceneNode();
			 if(nodo){
				 std::cout<<nodo->getName()<<std::endl;
				 _bodies.erase(it);
				 delete aux;
				 _sceneMgr->destroySceneNode(nodo->getName());
				 it--;
			 }//Fin if
		 }//Fin if
	 }//Fin for
}//Fin deleteSlept


void PlayState::finPartida(){
	_endGame=true;
}
