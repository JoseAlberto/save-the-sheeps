#include "Animal.h"

Animal::Animal(Ogre::SceneNode *node){
	_state = -1;
	_node = node;
}

void Animal::changeObjetive(Ogre::Vector3 pos){
	Ogre::Vector3 ndir;
	pos.y=2;
	_objetive = pos;

	/* Lo hacemos mirar hacia esa direccion */
	_node->lookAt(pos, Ogre::Node::TS_WORLD, Ogre::Vector3::NEGATIVE_UNIT_X);
	/* Calculamos el vector direccion */
	ndir = (pos - _node->getPosition()) ;
	/* Lo normalizamos */
	_dir = ndir.normalisedCopy();

}

Ogre::Vector3 Animal::getObjetive(){
	return _objetive;
}

Ogre::SceneNode* Animal::getNode(){
	return _node;
}
